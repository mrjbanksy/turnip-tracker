package com.mrjbanksy.android.tt

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import java.lang.NumberFormatException
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getSavedValues()
        setUpTextChangeListeners()
        calculateTotalInvestment()
        calculateTotalSalePrice()
//        val morningCalendar = setUpMorningCalendar()
//        setUpMorningAlarm(morningCalendar)
//        val afternoonCalendar = setUpAfternoonCalendar()
//        val builder = setUpNotification()
//        with(NotificationManagerCompat.from(this)) {
//            notify(1, builder.build())
//        }
    }

    private fun setUpTextChangeListeners() {
        val prices = arrayListOf(
            R.id.mondayAM,
            R.id.mondayPM,
            R.id.tuesdayAM,
            R.id.tuesdayPM,
            R.id.wednesdayAM,
            R.id.wednesdayPM,
            R.id.thursdayAM,
            R.id.thursdayPM,
            R.id.fridayAM,
            R.id.fridayPM,
            R.id.saturdayAM,
            R.id.saturdayPM
        )

        for (price in prices) {
            findViewById<EditText>(price).addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    calculateTotalSalePrice()
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            })
        }

        findViewById<EditText>(R.id.purchasePrice).addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                calculateTotalInvestment()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        })
        findViewById<EditText>(R.id.numPurchased).addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                calculateTotalInvestment()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        })
    }

    private fun calculateTotalSalePrice() {
        var currentPrice: Int
        val dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
        val amOrPm = Calendar.getInstance().get(Calendar.AM_PM)
        try {
            if (dayOfWeek == Calendar.MONDAY && amOrPm == Calendar.AM) {
                currentPrice =
                    Integer.parseInt(findViewById<EditText>(R.id.mondayAM).text.toString())
            } else if (dayOfWeek == Calendar.MONDAY && amOrPm == Calendar.PM) {
                currentPrice =
                    Integer.parseInt(findViewById<EditText>(R.id.mondayPM).text.toString())
            } else if (dayOfWeek == Calendar.TUESDAY && amOrPm == Calendar.AM) {
                currentPrice =
                    Integer.parseInt(findViewById<EditText>(R.id.tuesdayAM).text.toString())
            } else if (dayOfWeek == Calendar.TUESDAY && amOrPm == Calendar.PM) {
                currentPrice =
                    Integer.parseInt(findViewById<EditText>(R.id.tuesdayPM).text.toString())
            } else if (dayOfWeek == Calendar.WEDNESDAY && amOrPm == Calendar.AM) {
                currentPrice =
                    Integer.parseInt(findViewById<EditText>(R.id.wednesdayAM).text.toString())
            } else if (dayOfWeek == Calendar.WEDNESDAY && amOrPm == Calendar.PM) {
                currentPrice =
                    Integer.parseInt(findViewById<EditText>(R.id.wednesdayPM).text.toString())
            } else if (dayOfWeek == Calendar.THURSDAY && amOrPm == Calendar.AM) {
                currentPrice =
                    Integer.parseInt(findViewById<EditText>(R.id.thursdayAM).text.toString())
            } else if (dayOfWeek == Calendar.THURSDAY && amOrPm == Calendar.PM) {
                currentPrice =
                    Integer.parseInt(findViewById<EditText>(R.id.thursdayPM).text.toString())
            } else if (dayOfWeek == Calendar.FRIDAY && amOrPm == Calendar.AM) {
                currentPrice =
                    Integer.parseInt(findViewById<EditText>(R.id.fridayAM).text.toString())
            } else if (dayOfWeek == Calendar.FRIDAY && amOrPm == Calendar.PM) {
                currentPrice =
                    Integer.parseInt(findViewById<EditText>(R.id.fridayPM).text.toString())
            } else if (dayOfWeek == Calendar.SATURDAY && amOrPm == Calendar.AM) {
                currentPrice =
                    Integer.parseInt(findViewById<EditText>(R.id.saturdayAM).text.toString())
            } else if (dayOfWeek == Calendar.SATURDAY && amOrPm == Calendar.PM) {
                currentPrice =
                    Integer.parseInt(findViewById<EditText>(R.id.saturdayPM).text.toString())
            } else {
                currentPrice = 0
            }
        } catch (e: NumberFormatException) {
            currentPrice = 0
        }
        if (currentPrice > 0) {
            val numberOfTurnips =
                Integer.parseInt(findViewById<EditText>(R.id.numPurchased).text.toString())
            val totalPrice = currentPrice * numberOfTurnips
            findViewById<TextView>(R.id.salePrice).text = "Sale Price: $totalPrice bells"
            val purchasePrice =
                Integer.parseInt(findViewById<TextView>(R.id.purchasePrice).text.toString())
            val numberPurchased =
                Integer.parseInt(findViewById<TextView>(R.id.numPurchased).text.toString())
            if (currentPrice >= purchasePrice) {
                findViewById<TextView>(R.id.profit).text =
                    "Profit: ${(currentPrice - purchasePrice) * numberPurchased} bells"
            } else {
                findViewById<TextView>(R.id.profit).text =
                    "Loss: ${(purchasePrice - currentPrice) * numberPurchased} bells"
            }
        } else {
            findViewById<TextView>(R.id.salePrice).text =
                "Sale Price unknown! Update the current turnip price."
            findViewById<TextView>(R.id.profit).text =
                "Profit unknown! Update the current turnip price."
        }
    }

    private fun setUpMorningAlarm(morningCalendar: Calendar) {
        val alarmIntent = Intent(this, AlarmReceiver::class.java).let { intent ->
            PendingIntent.getBroadcast(
                this,
                0,
                intent,
                0
            )
        }
        val alarmMgr = getSystemService(Context.ALARM_SERVICE) as? AlarmManager
        alarmMgr?.setInexactRepeating(
            AlarmManager.RTC_WAKEUP,
            morningCalendar.timeInMillis,
            AlarmManager.INTERVAL_DAY,
            alarmIntent
        )
    }

    private fun setUpMorningCalendar(): Calendar {
        return Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, 16)
            set(Calendar.MINUTE, 5)
        }
    }

    private fun setUpAfternoonCalendar(): Calendar {
        return Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, 12)
        }
    }

    private fun setUpNotification(): NotificationCompat.Builder {

        createNotificationChannel()

        val notificationIntent = Intent(this, NotificationActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

        return NotificationCompat.Builder(this, getString(R.string.channel_id))
            .setSmallIcon(R.drawable.turnip_notification)
            .setContentTitle(getString(R.string.notification_title))
            .setContentText(getString(R.string.notification_text))
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_id)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel =
                NotificationChannel(getString(R.string.channel_id), name, importance).apply {
                    description = descriptionText
                }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun save(view: View) {
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putInt(
                getString(R.string.purchasePrice),
                Integer.parseInt(findViewById<EditText>(R.id.purchasePrice).text.toString())
            )
            putInt(
                getString(R.string.numPurchased),
                Integer.parseInt(findViewById<EditText>(R.id.numPurchased).text.toString())
            )
            putInt(
                getString(R.string.mondayAM),
                Integer.parseInt(findViewById<EditText>(R.id.mondayAM).text.toString())
            )
            putInt(
                getString(R.string.mondayPM),
                Integer.parseInt(findViewById<EditText>(R.id.mondayPM).text.toString())
            )
            putInt(
                getString(R.string.tuesdayAM),
                Integer.parseInt(findViewById<EditText>(R.id.tuesdayAM).text.toString())
            )
            putInt(
                getString(R.string.tuesdayPM),
                Integer.parseInt(findViewById<EditText>(R.id.tuesdayPM).text.toString())
            )
            putInt(
                getString(R.string.wednesdayAM),
                Integer.parseInt(findViewById<EditText>(R.id.wednesdayAM).text.toString())
            )
            putInt(
                getString(R.string.wednesdayPM),
                Integer.parseInt(findViewById<EditText>(R.id.wednesdayPM).text.toString())
            )
            putInt(
                getString(R.string.thursdayAM),
                Integer.parseInt(findViewById<EditText>(R.id.thursdayAM).text.toString())
            )
            putInt(
                getString(R.string.thursdayPM),
                Integer.parseInt(findViewById<EditText>(R.id.thursdayPM).text.toString())
            )
            putInt(
                getString(R.string.fridayAM),
                Integer.parseInt(findViewById<EditText>(R.id.fridayAM).text.toString())
            )
            putInt(
                getString(R.string.fridayPM),
                Integer.parseInt(findViewById<EditText>(R.id.fridayPM).text.toString())
            )
            putInt(
                getString(R.string.saturdayAM),
                Integer.parseInt(findViewById<EditText>(R.id.saturdayAM).text.toString())
            )
            putInt(
                getString(R.string.saturdayPM),
                Integer.parseInt(findViewById<EditText>(R.id.saturdayPM).text.toString())
            )
            commit()
        }
    }

    private fun getSavedValues() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        findViewById<EditText>(R.id.purchasePrice).setText(
            sharedPref.getInt(
                getString(R.string.purchasePrice),
                0
            ).toString()
        )
        findViewById<EditText>(R.id.numPurchased).setText(
            sharedPref.getInt(
                getString(R.string.numPurchased),
                0
            ).toString()
        )
        findViewById<EditText>(R.id.mondayAM).setText(
            sharedPref.getInt(
                getString(R.string.mondayAM),
                0
            ).toString()
        )
        findViewById<EditText>(R.id.mondayPM).setText(
            sharedPref.getInt(
                getString(R.string.mondayPM),
                0
            ).toString()
        )
        findViewById<EditText>(R.id.tuesdayAM).setText(
            sharedPref.getInt(
                getString(R.string.tuesdayAM),
                0
            ).toString()
        )
        findViewById<EditText>(R.id.tuesdayPM).setText(
            sharedPref.getInt(
                getString(R.string.tuesdayPM),
                0
            ).toString()
        )
        findViewById<EditText>(R.id.wednesdayAM).setText(
            sharedPref.getInt(
                getString(R.string.wednesdayAM),
                0
            ).toString()
        )
        findViewById<EditText>(R.id.wednesdayPM).setText(
            sharedPref.getInt(
                getString(R.string.wednesdayPM),
                0
            ).toString()
        )
        findViewById<EditText>(R.id.thursdayAM).setText(
            sharedPref.getInt(
                getString(R.string.thursdayAM),
                0
            ).toString()
        )
        findViewById<EditText>(R.id.thursdayPM).setText(
            sharedPref.getInt(
                getString(R.string.thursdayPM),
                0
            ).toString()
        )
        findViewById<EditText>(R.id.fridayAM).setText(
            sharedPref.getInt(
                getString(R.string.fridayAM),
                0
            ).toString()
        )
        findViewById<EditText>(R.id.fridayPM).setText(
            sharedPref.getInt(
                getString(R.string.fridayPM),
                0
            ).toString()
        )
        findViewById<EditText>(R.id.saturdayAM).setText(
            sharedPref.getInt(
                getString(R.string.saturdayAM),
                0
            ).toString()
        )
        findViewById<EditText>(R.id.saturdayPM).setText(
            sharedPref.getInt(
                getString(R.string.saturdayPM),
                0
            ).toString()
        )
    }

    fun calculateTotalInvestment() {
        var purchasePrice: Int
        var numPurchased: Int
        try {
            purchasePrice =
                Integer.parseInt(findViewById<EditText>(R.id.purchasePrice).text.toString())
            numPurchased =
                Integer.parseInt(findViewById<EditText>(R.id.numPurchased).text.toString())
        } catch (e: NumberFormatException) {
            purchasePrice = 0
            numPurchased = 0
        }
        findViewById<TextView>(R.id.totalInvestment).text =
            "Total Investment: ${purchasePrice.times(numPurchased)} bells"
    }

    fun clear(view: View) {
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putInt(getString(R.string.purchasePrice), 0)
            putInt(getString(R.string.numPurchased), 0)
            putInt(getString(R.string.mondayAM), 0)
            putInt(getString(R.string.mondayPM), 0)
            putInt(getString(R.string.tuesdayAM), 0)
            putInt(getString(R.string.tuesdayPM), 0)
            putInt(getString(R.string.wednesdayAM), 0)
            putInt(getString(R.string.wednesdayPM), 0)
            putInt(getString(R.string.thursdayAM), 0)
            putInt(getString(R.string.thursdayPM), 0)
            putInt(getString(R.string.fridayAM), 0)
            putInt(getString(R.string.fridayPM), 0)
            putInt(getString(R.string.saturdayAM), 0)
            putInt(getString(R.string.saturdayPM), 0)
            commit()
        }
        findViewById<EditText>(R.id.purchasePrice).setText("0")
        findViewById<EditText>(R.id.numPurchased).setText("0")
        findViewById<EditText>(R.id.mondayAM).setText("0")
        findViewById<EditText>(R.id.mondayPM).setText("0")
        findViewById<EditText>(R.id.tuesdayAM).setText("0")
        findViewById<EditText>(R.id.tuesdayPM).setText("0")
        findViewById<EditText>(R.id.wednesdayAM).setText("0")
        findViewById<EditText>(R.id.wednesdayPM).setText("0")
        findViewById<EditText>(R.id.thursdayAM).setText("0")
        findViewById<EditText>(R.id.thursdayPM).setText("0")
        findViewById<EditText>(R.id.fridayAM).setText("0")
        findViewById<EditText>(R.id.fridayPM).setText("0")
        findViewById<EditText>(R.id.saturdayAM).setText("0")
        findViewById<EditText>(R.id.saturdayPM).setText("0")
    }

}
